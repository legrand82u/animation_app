# Première version

La première version des animation, la plus simple, et la moins optimisée, avec le setState en plein milieu de la page, qui est donc dans 'basic_animation_page.dart'

Tutoriel suivi : 'https://www.youtube.com/watch?v=txLvvlooT20'

## Seconde version 

La seconde version sépare la partie animée (le Transform.rotate(), du reste de la page, permettant de supprimer le setState lourd.)

Cela permet d'avoir un code plus optimisé, avec un AnimationWidget.

Tutoriel suivi : 'https://www.youtube.com/watch?v=o-h_e4b71o8'

## Troisième version

La troisième version, issue du même tutoriel vidéo, permet d'utiliser un AnimatedBuilder, séparant encore plus les choses animées, alors qu'avant on devait render à chaque fois l'image qui tourne, ici, non, seul le transform est redesign (pourtant l'image est son widget enfant), tout cela est possible grace à AnimatedBuilder.

Pour voir tout ça, il suffit de changer l'import dans le main, de la page que l'on veut voir basic, animated builder, ou animated widget. Le visuel est le même.